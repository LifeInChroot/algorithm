package main

import (
	"./queue"
	"fmt"
)

func main() {
	fmt.Println("FIFO: ")
	fifo := queue.NewFIFO()
	fifo.Push(1)
	fifo.Push(2)
	fifo.Push(3)

	for fifo.Len() != 0 {
		fmt.Println(fifo.Pop())
	}

	fmt.Println("LIFO: ")
	lifo := queue.NewLIFO()
	lifo.Push(1)
	lifo.Push(2)
	lifo.Push(3)

	for lifo.Len() != 0 {
		fmt.Println(lifo.Pop())
	}
}
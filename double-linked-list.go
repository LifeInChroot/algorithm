package main

import (
	"./double-linked-list"
	"fmt"
)

func printDblList(dblList *double_linked_list.List) {
	for e := dblList.Head(); e != nil; e = e.Next() {
		fmt.Println("======================================================")
		fmt.Println("< Prev  ", e.Prev())
		fmt.Println("* My adr", e, "[value: ", e.Value, "]")
		fmt.Println("> Next  ", e.Next())
	}
}

func main() {
	dblList := double_linked_list.NewList()

	dblList.PushFront(&double_linked_list.Item{Value: 0})
	dblList.PushFront(&double_linked_list.Item{Value: 1})
	dblList.PushFront(&double_linked_list.Item{Value: 2})
	dblList.PushBack(&double_linked_list.Item{Value: -1})
	dblList.PushBack(&double_linked_list.Item{Value: -2})
	dblList.PushBack(&double_linked_list.Item{Value: -3})
	dblList.PushFront(&double_linked_list.Item{Value: 3})

	// удаляем по элементу сначала и с конца
	dblList.Remove(dblList.Tail())
	dblList.Remove(dblList.Head())
	printDblList(dblList)

	// pop элемента с начала
	i := dblList.PopFront()
	fmt.Println(i.Value)
	printDblList(dblList)

	// pop элемента с конца
	i = dblList.PopBack()
	fmt.Println(i.Value)

	fmt.Println(dblList.Len())
}

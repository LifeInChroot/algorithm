package queue

import (
	"sync"
	"../double-linked-list"
)

type LIFO struct {
	q double_linked_list.List
	l sync.Mutex
}

func NewLIFO() *LIFO {
	return &LIFO{q: *double_linked_list.NewList()}
}

func (f *LIFO) Push(value interface{}) {
	f.l.Lock()
	f.q.PushBack(&double_linked_list.Item{Value: value})
	f.l.Unlock()
}

func (f *LIFO) Len() int32 {
	f.l.Lock()
	defer f.l.Unlock()
	return f.q.Len()
}

func (f *LIFO) Pop() interface{} {
	f.l.Lock()
	defer f.l.Unlock()
	return f.q.PopBack().Value
}

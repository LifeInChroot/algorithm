package queue

import (
	"sync"
	"../double-linked-list"
)

type FIFO struct {
	q double_linked_list.List
	l sync.Mutex
}

func NewFIFO() *FIFO {
	return &FIFO{q: *double_linked_list.NewList()}
}

func (f *FIFO) Push(value interface{}) {
	f.l.Lock()
	f.q.PushFront(&double_linked_list.Item{Value: value})
	f.l.Unlock()
}

func (f *FIFO) Len() int32 {
	f.l.Lock()
	defer f.l.Unlock()
	return f.q.Len()
}

func (f *FIFO) Pop() interface{} {
	f.l.Lock()
	defer f.l.Unlock()
	return f.q.PopBack().Value
}
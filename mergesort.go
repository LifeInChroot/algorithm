package main

import (
	"./mergesort"
	"fmt"
)

func main() {
	// низкоуровневые данные
	list := []string{"6", "b", "3", "5", "9", "d", "2", "1", "a", "2", "8", "4", "7", "0", "c"}

	// преобразуем в массив интерфейсов
	new := make([]interface{}, len(list))
	for i, v := range list {
		new[i] = v
	}

	fmt.Println("Unsorted:")
	fmt.Println(list)

	fmt.Println("\nSorted:")
	newList := mergesort.MSort(new)
	fmt.Println(newList)
}

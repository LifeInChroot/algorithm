package main

import (
	"./graph"
	"fmt"
)

func main() {
	g := graph.NewGraph()
	g.NewNode(1, "a")
	g.NewNode(2, "b")
	g.NewNode(3, "c")
	g.NewNode(4, "d")
	g.NewNode(5, "e")
	g.NewNode(6, "f")

	// a
	g.NewEdge(1, 6, 20)
	g.NewEdge(1, 3, 9)
	g.NewEdge(1, 2, 7)

	// b
	g.NewEdge(2, 3, 10)
	g.NewEdge(2, 4, 15)

	// c
	g.NewEdge(3, 4, 11)
	g.NewEdge(3, 5, 30)
	g.NewEdge(3, 6, 2)

	// d
	g.NewEdge(4, 5, 2)

	// e
	g.NewEdge(5, 6, 9)

	fmt.Println(g)
}

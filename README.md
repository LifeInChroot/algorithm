Реализации алгоритмов на go
===========================

double-linked-list
------------------

Содержит реализацию двусвязного списка. Дли инициализации списка существует функция `NewList()`.
Также реализованы следующие методы:

  * PushFront() - помещает элемент в начало списка
  * PopFront() - достает элемент из начала списка
  * PushBack() - помещает элемент в конец списка
  * PopBack()  - достает элемент из конца списка
  * Remove() - удаляет элемент из списка
  * Head() - возвращает элемент головы
  * Tail() - возвращает элемент хвоста
  * Len()  - возвращает длину списка (чтобы не обходить весь список и уменьшить сложность с O(n))


Пример:

	dblList := double_linked_list.NewList()

	dblList.PushFront(&double_linked_list.Item{Value: 0})
	dblList.PushFront(&double_linked_list.Item{Value: 1})
	dblList.PushBack(&double_linked_list.Item{Value: -1})
	
	for e := dblList.Head(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}
	
	i := dblList.PopBack()
	fmt.Println(i.Value)


queue
-----

Реализация очередей FIFO и LIFO. Для инициализации списка существуют соотетствующие функции `NewFIFO()` и `NewLIFO()`.

Оба класса имеют методы:

  * Push() - поместить элемент в очередь
  * Pop() - достать элемент из очереди
  * Len() - проверить длину очереди

Пример:

    fifo := queue.NewFIFO()
	fifo.Push(1)
	fifo.Push(2)
	fifo.Push(3)

	for fifo.Len() != 0 {
		fmt.Println(fifo.Pop())
	}


quicksort
---------

Реализация алгоритма QuickSort. Для сортировки используется функция `QSort()`, принимающая на вход массив интерфейсов.
Поскольку используется принцип "разделяй и влавствуй", то сложность алгоритма = O(n log n).

Пример:

	list := []string{"6", "b", "3", "5", "9", "d", "2", "1", "a", "2", "8", "4", "7", "0", "c"}

	// преобразуем в массив интерфейсов
	new := make([]interface{}, len(list))
	for i, v := range list {
		new[i] = v
	}
	
	fmt.Println(quicksort.QSort(new))
	

mergesort
---------

Реализация алгоритма сортировки слиянием. Для сортировки используется функция `MSort()`, принимающая на вход массив интерфейсов.
Поскольку используется принцип "разделяй и влавствуй", то сложность алгоритма = O(n log n).


Пример:

	list := []string{"6", "b", "3", "5", "9", "d", "2", "1", "a", "2", "8", "4", "7", "0", "c"}

	// преобразуем в массив интерфейсов
	new := make([]interface{}, len(list))
	for i, v := range list {
		new[i] = v
	}
	
	fmt.Println(quicksort.MSort(new))
	

binary search
-------------

Алгоритм двоичного поиска в заранее отсортированных данных. Для поиска элемента используется функция `Find()`.

Пример:

	list := []int64{2, 3, 5, 7, 53, 59, 61, 67, 71, 73, 89, 97}

	var value *int64
	if value = binary_search.Find(list, 5); value == nil {
		fmt.Println("Not found")
		return
	}
	fmt.Println(*value)

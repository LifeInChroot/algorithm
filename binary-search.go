package main

import (
	"./binary-search"
	"fmt"
)

func main() {
	list := []int64{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}

	var value *int64
	if value = binary_search.Find(list, 5); value == nil {
		fmt.Println("Not found")
		return
	}
	fmt.Println(*value)
}
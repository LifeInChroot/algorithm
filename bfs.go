package main

import (
	"fmt"
	"./graph"
	bfs "./breadth-first-search"
)

func main() {
	g := graph.NewGraph()
	g.NewNode(1, "a")
	g.NewNode(2, "b")
	g.NewNode(3, "c")
	g.NewNode(4, "d")
	g.NewNode(5, "e")
	g.NewNode(6, "f")
	g.NewNode(7, "g")
	g.NewNode(8, "h")

	// a
	g.NewEdge(1, 2, 7)
	g.NewEdge(1, 3, 9)

	// b
	g.NewEdge(2, 4, 15)
	g.NewEdge(2, 5, 10)

	// c
	g.NewEdge(3, 6, 11)
	g.NewEdge(3, 7, 30)

	g.NewEdge(5, 8, 9)

	b := bfs.NewBFS(g)

	if node := b.Find(1, 8); node == nil {
		fmt.Println("not found")
	} else {
		fmt.Println(node.Value)
	}
}